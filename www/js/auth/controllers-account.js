angular.module('app.controllers-account', [])
 
.controller('signUpCtrl', function($scope,$rootScope,$ionicSideMenuDelegate,
                                   $state, $stateParams,fireBaseData,$ionicHistory,sharedUtils, AuthService, Profile, Codes, Utils) {

	$scope.$on('$ionicView.enter', function(e) {  
		broadcastAuthChange();
	});
	 
    $scope.doSignUp = function (formName, signUpCred) {
	  
		if (formName.$valid) {  
		
		    sharedUtils.showLoading(); 
			
			AuthService.signUpEmail(signUpCred).then(
				function(AuthData){ 
					 //checkAuth();
					AuthService.getAuthState().then(
						function(user){
							$scope.checkAuth();
							//sharedUtils.showMessage("","<pre>"+JSON.stringify(user, undefined, 4));
							//refirectTo
						}
					);
					//$scope.checkAuth();   
					
					//checkAuth();
					/*
					      firebase.auth().onAuthStateChanged(function(user) {
						 
							if (user) {
							  // User is signed in. 
					 
							  $ionicHistory.nextViewOptions({
								historyRoot: true,
								disableAnimate: true,
								disableBack: true
							  }); 
							  $ionicSideMenuDelegate.canDragContent(true);  // Sets up the sideMenu dragable
							  $rootScope.extras = true; 
							  sharedUtils.hideLoading();
							  $state.go('tab.dash', {}, {location: "replace"});
							  sharedUtils.showAlert("Success Registered");							  
							  
							} else {
			 
							}
						 
						  });*/
	  
				}, function(error){
					sharedUtils.hideLoading();
					Codes.handleError(error)
				}
			)
 
		}  
		 
    }
	
	function broadcastAuthChange() {
		$rootScope.$broadcast('rootScope:authChange', {});
	};
})  

.controller('loginCtrl', function($scope,$rootScope,$ionicSideMenuDelegate,
                                   $state, $stateParams,fireBaseData,$ionicHistory,sharedUtils , AuthService, Profile, Codes, Utils, $ionicModal, Popup) {

	$scope.$on('$ionicView.enter', function(e) { 
		broadcastAuthChange();
	});
	
	/*$scope.checkAuth = function(){  
		AuthService.getAuthState().then(
			function(user){  
				if(user.hasOwnProperty('uid')){	
					var provider = "";   
					
					if(user){ 
						var emailVerified = user.emailVerified; 
						var provider = user.providerData[0].providerId;

						$ionicHistory.nextViewOptions({
							historyRoot: true,
							disableAnimate: true,
							disableBack: true
						}); 
						
						if(provider!=("facebook.com")) {
							if (!emailVerified || emailVerified==false) { 
								$state.go('verifyEmail', {}, {location: "replace"}); 
							}else{
								$state.go('menu.courts', {}, {location: "replace"});  
							}
						}else{   
							$state.go('menu.courts', {}, {location: "replace"});
						} 						
					} 	 
				}	

				sharedUtils.hideLoading(); 
			} 
		).catch(
			function(error){
				//alert(error);
				//Codes.handleError();
				//console.log("not Logged In");
			}
		);
	}			*/					   
	$scope.doLogin = function(formName, signInData) { 
		if (formName.$valid) {  	
		    sharedUtils.showLoading(); 	 
			if(formName.email && formName.password) {  
				AuthService.signInEmail(signInData).then(
				function(AuthData){  
                    $state.go('menu.courts', {}, {reload: true}); 
				} 
			  ).catch(function(error){
				  Codes.handleError(error); 
			  }); 
			}
		}
	};
	
	$scope.facebookLogin = function(){ 
		AuthService.facebookAuth();  
	};	
	
	function broadcastAuthChange() {
		$rootScope.$broadcast('rootScope:authChange', {});
	};

	// Forgot Password
	$scope.resetPass = {email: ''}
	let exitForgPassModal = () => {
		$scope.resetPass.email = ''
		$scope.forgPassModal.hide()
	}

	$ionicModal
    .fromTemplateUrl('forgot-password-modal', {scope: $scope, animation: 'slide-in-up'})
    .then(modal => $scope.forgPassModal = modal)

  $scope.resetPassword = () => {
  	AuthService.resetPassword($scope.resetPass.email).then(() => {
  		Popup.alert('Forgot Password', 'Please check your email for instructions in resetting your password.', exitForgPassModal)
  	}).catch(error => {
  		Popup.alert('Error', 'There is no account associated with this email.', exitForgPassModal)
  	})
  }
}) 

.controller('verifyEmailCtrl', function($scope,$rootScope,$ionicSideMenuDelegate,
                                   $state, $stateParams,fireBaseData,$ionicHistory,sharedUtils, AuthService, Codes, Utils) {
 
	$scope.verifyEmail = function(){  
		AuthService.verifyEmail();  
    } 
  
}) 
 
.controller('homeCtrl', function (
  $rootScope, $scope, $state, $stateParams, $timeout, 
  $ionicModal, $ionicHistory, $ionicPopup, $ionicActionSheet,AuthService, Codes, Utils, sharedUtils) {

  	$scope.$on('$ionicView.enter', function(e) {  
		broadcastAuthChange();
	});
	 
	$scope.facebookLogin = function(){ 		
		AuthService.facebookAuth();  
	};

	$scope.currentYear = new Date().getFullYear();
	
	function broadcastAuthChange() {
		$rootScope.$broadcast('rootScope:authChange', {});
	};	
})

.controller('AccountCtrl', function($scope,$rootScope, $ionicSideMenuDelegate,
                                   $state, $stateParams, fireBaseData, $ionicHistory, $ionicLoading, $ionicModal, sharedUtils, AuthService, Profile, Codes, Utils, Popup) {

	$scope.logout = function() {
		AuthService.logout(); 
		$state.go('home');
	};  
  
	/*$scope.$on('$ionicView.beforeEnter', function() { 
		$ionicLoading.show({
			template: '<ion-spinner icon="ripple"></ion-spinner>'
		});
	});*/

 	$scope.pageTitle = "My Account"; 
 
	$scope.profile = AuthService.ProfileData;
	$scope.profile.email = AuthService.AuthData.email;
	var photoUrl = $scope.profile.photo;
	/*if (photoUrl.indexOf("http://") == 0 || photoUrl.indexOf("https://") == 0) {
        $scope.profile.photo =  "data:image/jpeg;base64,"+profile.photo;
    }*/
	 
	
	$state.go('menu.account.info'); 
 	/**
	 * Photo and Full name
	 ***/ 
	 
	$ionicModal.fromTemplateUrl('templates/popup/edit-mainProfile.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.editMainProfileModal = modal;
	});
	  
	$scope.editMainInfo = function() {  
		$scope.editMainProfileModal.show();
	};
	
	$scope.closeEditMainInfo = function() {
		$scope.editMainProfileModal.hide(); 
	};

	$scope.browse = {
		label: 'Uploading...',
		disabled: false
	}
	$scope.browseImage = () => {
		let fileBrowser = document.querySelector('#browse')
		let fileBrowseBtn = document.querySelector('#browseBtn')

		fileBrowser.onchange = () => {
			let file = fileBrowser.files[0]
			let uploadTask = firebase.storage().ref().child(file.name).put(file)

			fileBrowseBtn.disabled = true

			uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, snapshot => {
				let progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
				fileBrowseBtn.innerText = `Uploading... ${progress}%`
			}, error => {
				Popup.alert('Upload Failed', 'An error occured. Please try again.', () => {
					fileBrowser.value = null
					$scope.editMainProfileModal.hide()
					fileBrowseBtn.disabled = false
					fileBrowseBtn.innerText = 'Browse Photo'
				})
				console.log('UPLOAD ERROR:', error)
			}, () => {
				let imageUrl = uploadTask.snapshot.downloadURL
				let {currentUser} = firebase.auth()

				firebase.database().ref(`users/${currentUser.uid}/photo`).set(imageUrl).then(() => {
					$scope.editMainProfileModal.hide()
					fileBrowseBtn.disabled = false
					fileBrowseBtn.innerText = 'Browse Photo'
				})
			})
		}

		fileBrowser.click()
	}

	 /* $scope.uploadProfilePhoto = function() {
		Camera.gallery().then(function(imageURI) {
		  $scope.team.image = imageURI;
		});
	  };	 */
	/*
	
	//$scope.AuthData = AuthService.AuthData; 
 
	AuthService.getAuthState().then(	
		function(AuthData){
			$scope.currentUser = AuthData;   
			$scope.profile = Profile.getById(AuthData.uid);
 
			$scope.profile.$loaded().then(function(data) {   
			
				$scope.profile.email = AuthData.email;
				
				if(!$scope.profile.photo){
					$scope.profile.photo = "img/system/default-user.png";
				}	
				
				if(!$scope.profile.mobile){
					$scope.profile.mobile = "";
				}	
				
				if(!$scope.profile.teamInfo){
					//$scope.profile.teamInfo = "Silent H - Point Guard";
					$scope.profile.teamInfo = "";
				}				
			});
			
			  $ionicLoading.hide(); 
		}
	); */

 
})

.controller('AccountInfoCtrl', function($scope,$rootScope, $ionicSideMenuDelegate,
                                   $state, $stateParams, $ionicModal, fireBaseData, $ionicHistory, $ionicLoading, ionicDatePicker, sharedUtils, AuthService, Profile, Codes, Utils, Chats, UserProfile, Popup) {

	$scope.$on('$ionicView.afterEnter', function(e) {
		$ionicLoading.hide();
	});

	$scope.logout = function() {
		AuthService.logout(); 
		$state.go('home');
	};  
	
	$scope.profile = AuthService.ProfileData;
	$scope.email = AuthService.AuthData.email;
	var address = AuthService.ProfileData.address;
 
	if (address) {
		if(address.line1){
			$scope.profile.line1 = address.line1;
			$scope.profile.full_address =   address.line1+", ";
		}
		if(address.baranggay){	
			$scope.profile.baranggay = address.baranggay;
			$scope.profile.full_address +=  address.baranggay+", ";
		}
		if(address.city){
			$scope.profile.city = address.city;
			$scope.profile.full_address +=  address.city+", ";
		}
		if(address.spr){
			$scope.profile.spr = address.spr;
			$scope.profile.full_address +=  address.spr+", ";
		}
		if(address.zip){
			$scope.profile.zip = address.zip;
			$scope.profile.full_address +=  address.zip+" ";
		}
		if(address.country){
			$scope.profile.country = address.country;
			$scope.profile.full_address +=  address.country;
		}
	}
	
	/**
	 * Address and other info
	 ***/
	$ionicModal.fromTemplateUrl('templates/popup/edit-profile.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.editProfileModal = modal;
	});
 
	$scope.editInfo = function( ) {  
		$scope.editProfileModal.show();
	};
	
	$scope.closeEditInfo = function() {
		$scope.editProfileModal.hide(); 
	};	
	
	
	$scope.openDatePicker1 = function(){
		var dataPickerOpt = {
			callback: function (val) {  
			  $scope.profile.birthday = Utils.formatDate(new Date(val));
			},  
			inputDate: new Date(),  
			dateFormat: 'MM/dd/yyyy',
			mondayFirst: false,
			templateType: 'modal'
		};		
		ionicDatePicker.openDatePicker(dataPickerOpt);
    };	
	 
	$scope.updateUserInfo = function(formName) {

		if(formName.$valid) {
			$scope.uid = AuthService.AuthData.uid;

			var recordProfile = UserProfile.getById($scope.uid); 
			recordProfile.$loaded().then(function() {	 
				//console.log($scope.profile);
				recordProfile.address = {};
				recordProfile.birthday = $scope.profile.birthday;
				recordProfile.school = $scope.profile.school;
				recordProfile.work = $scope.profile.work;
				recordProfile.interests = $scope.profile.interests;
				recordProfile.hobbies = $scope.profile.hobbies;
				recordProfile.address.line1= $scope.profile.line1;
				recordProfile.address.baranggay= $scope.profile.baranggay;
				recordProfile.address.city= $scope.profile.city;
				recordProfile.address.spr= $scope.profile.spr;
				recordProfile.address.zip= $scope.profile.zip;
				recordProfile.address.country= $scope.profile.country; 
				
				recordProfile.$save().then(function(ref) {
					console.log("Saved");
					  Popup.alert('Update Account', 'Account Info successfully updated.', function() {
						$scope.closeEditInfo();	
						$state.go($state.current, {}, {reload: true}); 
					  });			
						 
				});
			});
		
		}

	};
	
		 // $ionicLoading.hide(); 
  
 }) 

.controller('AccountHistoryCtrl', function($scope,$rootScope, $ionicSideMenuDelegate,
                                   $state, $stateParams, fireBaseData, $ionicHistory, $ionicLoading, sharedUtils, AuthService, Profile, Codes, Utils) {

 }) 

.controller('AccountPostsCtrl', function($scope,$rootScope, $ionicSideMenuDelegate,
                                   $state, $stateParams, fireBaseData, $ionicHistory, $ionicLoading, sharedUtils, AuthService, Profile, Codes, Utils) {
 
 }) 

.controller('AccountPhotosCtrl', function($scope,$rootScope, $ionicSideMenuDelegate,
                                   $state, $stateParams, fireBaseData, $ionicHistory, $ionicLoading, sharedUtils, AuthService, Profile, Codes, Utils) {
 
 }) 

.controller('AccountWalletCtrl', function($scope,$rootScope, $ionicSideMenuDelegate,
                                   $state, $stateParams, fireBaseData, $ionicHistory, $ionicLoading, sharedUtils, AuthService, Profile, Codes, Utils) {
 
 })  
 