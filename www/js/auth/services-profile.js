angular.module('app.services-profile', [])

.factory('Profile', function($firebaseArray, $firebaseObject, Utils, Codes, sharedUtils) {
  /*var self = this;

  self.ProfileData = {};
  
   // GET  users/$uid
  self.getUser = function(uid) {
    var childRef = "users/" + uid; 
    return firebaseFunc.onValue(childRef).then(
      function(ProfileData){  
        self.ProfileData = ProfileData;
        return ProfileData;
      },
      function(error){
        return error;
      }
    )
  }; 

  return self;*/
  
  var userRef = firebase.database().ref().child('users'); 
	
  return {
    get: function() { 
      return $firebaseArray(userRef); //get all courts
    },
    getById: function(uid) {   
      var user = userRef.child(uid); 		  
      return $firebaseObject(user);
    },
	setGlobal: function(uid, globalProperty, globalValue) {
		var childRef = "users/" + uid +"/" + globalProperty;
		return FireFunc.set(childRef, globalValue).then(
		  function(successCallback){
			Utils.showMessage("Profile updated", 750);
			return successCallback;
		  },
		  function(error){
			Codes.handleError(error);
			return error;
		  }
		);
	},
	  
	setSub: function(uid, globalProperty, subProperty, subValue) {
		var childRef = "users/" + uid +"/" + globalProperty + "/" + subProperty;
		return FireFunc.set(childRef, subValue).then(
		  function(successCallback){
			return successCallback;
		  },
		  function(error){
			Codes.handleError(error);
			return error;
		  }
		);
	}	
  };
  
})