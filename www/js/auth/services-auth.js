angular.module('app.services-auth', [])

/**
 * General Wrapper for Auth
 */
.factory('AuthService', function($q, $state, $stateParams, $rootScope, $ionicSideMenuDelegate, $ionicHistory, Profile, Codes, fireBaseData, sharedUtils, Utils) {

	var self = this;
	self.isAuthenticated = false;  
	self.AuthData = {};  
	self.ProfileData = {};  
	self.valid = false;  

	onAuth().then(
		function(AuthData){
			self.AuthData = AuthData;
 
			var profile = Profile.getById(AuthData.uid);
			
			profile.$loaded().then(function(data) {    
				self.ProfileData = data; 
			}); 
			
		}
	);
  
	function onAuth() { 
		var qCheck = $q.defer();
		firebase.auth().onAuthStateChanged(
		  function(user) {
			if (user) { 
				self.AuthData = user;  
				qCheck.resolve(user); 
				self.getUserCredentials();
			} else {  
				console.log("logged out");
				broadcastAuthChange();
				qCheck.reject("AUTH_LOGGED_OUT");  
				$state.go('home', {}, {location: "replace"});
			};
		});
		
		return qCheck.promise;
	};
	 
	self.getAuthState = function() {
		return onAuth();
	};
 
 	self.getUserCredentials = function(){
		
		var user = firebase.auth().currentUser;
		console.log("getusercred");
		
		if (user != null) { 

			var providerId = user.providerData[0].providerId;
			var userProfile = Profile.getById(user.uid);
  
	 
			userProfile.$loaded().then(function(data) {   
			
				self.ProfileData = data;
				
				if(!data.hasOwnProperty("full_name")){
					console.log("adding info");
					self.addUserInfo(user,'login',providerId);
				}else{
					console.log("already have info");
				} 
				 
			}); 
  
			self.refirectTo(user); 
		}			
	};
 
	
	self.addUserInfo = function(user,flow,provider,addtnlInfo){
 
		if(flow=="login" && provider=="facebook.com"){
			var full_name = user.displayName;
			var photo = user.photoURL;   
			var mobile = "";
		}else if(flow=="signup" && provider=="password"){
			var full_name = addtnlInfo.name;
			var photo = "img/system/default-user.png";   
			var mobile = addtnlInfo.mobile;			
		}
		 
		fireBaseData.refFirebaseDBUser().child(user.uid).set({ 
		  did:Utils.genRandomCode(user.uid),
		  full_name: full_name, 
		  mobile: mobile,
		  role: "player", 
		  photo: photo,   
		  birthday: "",
		  school: "",
		  work: "",
		  hobbies: "",
		  interests: "",
		});			
		fireBaseData.refFirebaseDBUser().child(user.uid).child("address").set({ 	
			line1: "",
			baranggay: "",
			city: "",
			spr: "",
			country: "",
			zip: "",			
		});				
	 
	};	
	
	self.checkIfRegistered = function(userId){
		var user = Profile.getById(userId);
		if(user.$id){
			return true;
		}else{
			return false;
		} 
	};
	
	self.unAuth = function() {
		var qSignOut = $q.defer();
		firebase.auth().signOut().then(function() { 
		  broadcastAuthChange();
		  qSignOut.resolve();
		}, function(error) { 
		  qSignOut.reject(error);
		});
		return qSignOut.promise;
	};
	
	self.refirectTo = function(user){
		 //FirebaseAuthProvider
		 console.log("redirectTo");
		 provider = user.providerData[0].providerId; 
		 
		 //sharedUtils.hideLoading();
		 
		 if(provider!= "facebook.com"){ 
	 
			if(user.emailVerified){ 
				$ionicHistory.nextViewOptions({
					historyRoot: true,
					disableAnimate: true,
					disableBack: true
				}); 	
				$state.go('menu.courts', {}, {location: "replace"});
			}else{ 
				$ionicHistory.nextViewOptions({
					historyRoot: true,
					disableAnimate: true,
					disableBack: true
				}); 	
				$state.go('verifyEmail', {}, {location: "replace"});						
			}
		 
		}else{ 
			$state.go('menu.courts', {}, {location: "replace"});
		} 
	} 
 
	self.logout = function(){ 
	  if (firebase.auth().currentUser) { 
        firebase.auth().signOut();  
      }		
	};

 
	self.verifyEmail = function(){
      firebase.auth().currentUser.sendEmailVerification().then(function() { 
		var buttonInfo = { 
              text: 'OK',
              type: 'button-block button-stable',
              scope: null,
              onTap: function(e) {
					self.logoutEmail();
              } 
		};	  
		var userData = self.AuthData;
		sharedUtils.showMessage('','The verification link was sent to <b>'+userData.email+'</b>. Please Check your email address and click the link to verify your account', 3000,'',buttonInfo); 

      }).catch(function(error){ 
			Codes.handleError(error); 
	  });
	   
	};
  
	self.signUpEmail = function(signUpCred) {
  
			var qAuth = $q.defer(); 
			firebase.auth().createUserWithEmailAndPassword(signUpCred.email, signUpCred.password)
			.then(function(User){ 
				
				//alert("email signin");
				sharedUtils.hideLoading(); 
				 
				//$state.go('menu.dash', {}, {location: "replace"});
				//$state.go($state.current, {}, {reload: true});
				
				self.AuthData = User; 
				qAuth.resolve(User);
				self.addUserInfo(User,'signup','password',signUpCred);
			})
			.catch(function(error) { 
			
				sharedUtils.hideLoading();
				Codes.handleError(error);  
			});
			
			return qAuth.promise;
		 
	};	

	self.signInEmail = function(signInData) {
  
			var qAuth = $q.defer();  
			firebase.auth().signInWithEmailAndPassword(signInData.email, signInData.password).then(function(user){ 
				self.AuthData = user;
				qAuth.resolve(user);
				sharedUtils.hideLoading();
				$state.go($state.current, {}, {reload: true});
			}) 
			.catch(function(error) { 
				sharedUtils.hideLoading();
				Codes.handleError(error);  
			}); 
			 
			return qAuth.promise;
		 
	};	
	
	self.logoutEmail = function(){
		if (firebase.auth().currentUser) { 
			firebase.auth().signOut(); 
			$state.go('login', {}, {location: "replace"}); 
		}
	};
	
	// FACEBOOK AUTH
	self.facebookAuth = function() { 
		// Start a sign in process for an unauthenticated user.
		var provider = new firebase.auth.FacebookAuthProvider();
		 
		//var token = result.credential.accessToken;
		provider.addScope('user_birthday');
		provider.addScope('public_profile'); 
		
		sharedUtils.showLoading("Signing in ....");
		
		firebase.auth().signInWithRedirect(provider).then(function(result){
			
				 if (result.credential) {
					// This gives you a Facebook Access Token. You can use it to access the Facebook API.
					var token = result.credential.accessToken;
					// ...
				  }
				  // The signed-in user info.
				 // alert(token);
				  var user = result.user;

			
			//var token = result.credential.accessToken;
			//self.AuthData = result.user;   
			
			/*fireBaseData.refFirebaseDBUser().child(result.user.uid).set({
			  did:Utils.genRandomCode(result.user.uid),
			  mobile: signUpCred.mobile,
			  full_name: signUpCred.name, 
			  role:"player"
			});			*/	
			//$state.go('menu.courts', {}, {location: "replace"});
			
		}).catch(function(error) {
			sharedUtils.hideLoading();
			//alert(error);
			Codes.handleError(error);  
		});
			
	
		/*	// Sign in using a redirect.
			firebase.auth().getRedirectResult().then(function(result) {
			  if (!result.credential) {
						
			  }else{
				  alert("test2");
				  var token = result.credential.accessToken; 
				  self.AuthData =  result.user;
			  }  
			}).catch(function(error) {
				sharedUtils.hideLoading();
				Codes.handleError(error);  
			})*/

			

		/*var provider = new firebase.auth.FacebookAuthProvider();
		//provider.addScope('user_birthday');
		//alert("test1");
		
		
		firebase.auth().$signInWithRedirect(provider)
		  .then(function(firebaseUser) {
				alert("test");
				sharedUtils.hideLoading(); 
				document.getElementById('quickstart-account-details').textContent = JSON.stringify(firebaseUser, null, '  '); 
		  })
		  .catch(function(error) {
				sharedUtils.hideLoading();
				Codes.handleError(error);  
		  })
		  
			firebase.auth().getRedirectResult().then(function (authData) {
			  alert("test");
			}).catch(function (error) {
			  Codes.handleError(error);  
			});*/
	};	



	  function broadcastAuthChange() {
		$rootScope.$broadcast('rootScope:authChange', {});
	  };

	// Forgot Password
	self.resetPassword = email => {
		return firebase.auth().sendPasswordResetEmail(email)
	}
  
	return self;
  
});