angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
 
  $stateProvider
    

  .state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: 'homeCtrl',
	/*resolve: {
	  currentAuth: ['Auth', function(Auth) {
		return Auth.$requireSignIn()
	  }]
	},	*/ 
  })

  .state('signUp', {
    url: '/sign-up',
    templateUrl: 'templates/signUp.html',
    controller: 'signUpCtrl' 
  })

  .state('verifyEmail', {
    url: '/verifyEmail',
    templateUrl: 'templates/verifyEmail.html',
    controller: 'verifyEmailCtrl' 
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl' 
  })
  // setup an abstract state for the tabs directive
  
 .state('menu', {
    url: '/menu', 
    abstract: true,
    templateUrl: 'templates/menu.html',
	controller: 'MenuCtrl' 	
  })

  // Each tab has its own nav history stack:
  .state('menu.dash', {
    url: '/dash',
    views: {
        'dash-tab': {
          templateUrl: 'templates/dashboard.html', 
        }
    }
  })
  
  .state('menu.courts', {
    url: '/courts',
    views: {
      'menu-courts': {
        templateUrl: 'templates/menu-courts.html',
        controller: 'CourtsCtrl'
      }
    } 	
  })

  .state('menu.court-list', {
    url: '/court-list',
    views: {
      'menu-courts': {
        templateUrl: 'templates/court-list.html',
        controller: 'CourtListCtrl'
      }
    } 
  })
  
  .state('menu.court-detail', {
    url: '/court/:court_id',
    views: {
      'menu-courts': {
        templateUrl: 'templates/court-detail.html',
        controller: 'CourtDetailCtrl'
      }
    } 
  }) 

  .state('menu.game-detail', {
    url: '/court/:game_id',
    views: {
      'menu-courts': {
        templateUrl: 'templates/game-detail.html',
        controller: 'GameDetailCtrl'
      }
    }
  }) 

  .state('menu.team-players', {
    url: '/team-players/:gameId',
    views: {
      'menu-courts': {
        templateUrl: 'templates/team-players.html',
        controller: 'TeamPlayersCtrl'
      }
    }
  })

  .state('menu.chats', {
      url: '/chats', 
      views: {
        'menu-chats': {
          templateUrl: 'templates/menu-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('menu.chat-detail', {
      url: '/chats/:chatId',
      params: {
        name: null
      },
      views: {
        'menu-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('menu.teams', {
    url: '/teams', 
    views: {
      'menu-teams': {
        templateUrl: 'templates/menu-teams.html',
        controller: 'TeamsCtrl'
      }
    }
  })

	.state('menu.team-detail', {
      url: '/teams/:team_id',
      views: {
        'menu-teams': {
          templateUrl: 'templates/team-detail.html',
          controller: 'TeamDetailCtrl'
        }
      }
    })
	
  .state('menu.shop', {
    url: '/shop', 
    views: {
      'menu-shop': {
        templateUrl: 'templates/menu-shop.html',
        controller: 'ShopCtrl'
      }
    }
  })

  .state('menu.account', {
    url: '/account',  
    views: {	
      'menu-account': {
        templateUrl: 'templates/menu-account.html',
        controller: 'AccountCtrl' 
	  }
	}, 
  })

  .state('menu.account.info', {
    url: '/info',  
    views: {	
      'menuContent': {
        templateUrl: 'templates/menu-account-info.html',
        controller: 'AccountInfoCtrl' 
	  }
	}
  })

  .state('menu.account.history', {
    url: '/history',  
    views: {	
      'menuContent': {
        templateUrl: 'templates/menu-account-history.html',
        controller: 'AccountHistoryCtrl' 
	  }
	}
  })

  .state('menu.account.posts', {
    url: '/posts',  
    views: {	
      'menuContent': {
        templateUrl: 'templates/menu-account-posts.html',
        controller: 'AccountPostsCtrl' 
	  }
	}
  })

  .state('menu.account.photos', {
    url: '/photos',  
    views: {	
      'menuContent': {
        templateUrl: 'templates/menu-account-photos.html',
        controller: 'AccountPhotosCtrl' 
	  }
	}
  })

  .state('menu.account.wallet', {
    url: '/wallet',  
    views: {	
      'menuContent': {
        templateUrl: 'templates/menu-account-wallet.html',
        controller: 'AccountWalletCtrl' 
	  }
	}
  })
  ;

   $urlRouterProvider.otherwise('/menu/home');


});