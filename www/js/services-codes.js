angular.module('app.services-codes', [])
 
/**
 * Handles error codes from Firebase and e.g.
 */
.factory('Codes', function(sharedUtils, $q) {
  var self = this;
  
  self.handleError = function(error) {
    sharedUtils.showMessage('',self.getErrorMessage(error), 1500);
  };
  
  self.getErrorMessage = function(error){
    var errorMessage = "";
    console.log(error)
	if (error.hasOwnProperty('code')){ 
      switch(error.code) {
		case 'auth/email-already-in-use':  
	      errorMessage = "Email Already exist";
		  break
		case 'auth/invalid-email':
		  errorMessage = "Email Address is not valid";
		  break	
		case 'auth/operation-not-allowed':
		  break		
		case 'auth/weak-password':
		  errorMessage = "The password is too weak."
		  break	
		case 'auth/user-not-found':
		  errorMessage = "Email is not yet registered. Please Register First"
		  break
		case 'auth/wrong-password':
		  errorMessage = "You provide invalid password. If you forgot your password please reset it"
		  break
        default:  
          errorMessage = "Oops. Something went wrong..."
          break
      }		
	}else{ 
		errorMessage = error.message;
	}
    /*if (error.hasOwnProperty('code')){
      switch(error.code) {
        case 'INVALID_USER':
          //
          updateMessage = "User does not exist... Sign up!"
          // perhaps an automatic redirect
          break
        case 'INVALID_EMAIL':
          //
          updateMessage = "Invalid E-mail. Try again"
          break
        case 'INVALID_PASSWORD':
          //
          updateMessage = "Incorrect password"
          break
        case 'INVALID_INPUT':
          //
          updateMessage = "Invalid E-mail or password. Try again"
          break
        case 'EMAIL_TAKEN':
          //
          updateMessage = "E-mail is already taken. Forgot password? Reset it"
          break
        case 'USERNAME_TAKEN':
          //
          updateMessage = "Username is already taken. Try again"
          break
        case 'USERNAME_NONEXIST':
          //
          updateMessage = "User not found. Check your spelling"
          break
        case 'PROFILE_NOT_SET':
          //
          updateMessage = "Please provide an username and display name"
          break
        case 'POST_NEW_CHAR_EXCEEDED':
          //
          updateMessage = "Your post can have max. " + POST_MAX_CHAR + " characters"
          break
        default: 
          //
          updateMessage = "Oops. Something went wrong..."
          break
      }
    } else {
      updateMessage = "Oops. Something went wrong..."
    }*/
    return errorMessage;
  };

	self.ValidateSignUpForm = function(signUpCred){  
		//sharedUtils.hideLoading(); 
		console.log(signUpCred);
		var validForm="";
		var ErrorTitle = "Error( Sign up )";
		var errorMessage ="";
		if (signUpCred.email.length < 1) {
			errorMessage = 'Please enter an email address.';
			validForm =  false;
		}else if (signUpCred.contact.length < 1) {
			errorMessage = 'Please enter a mobile.'; 
			validForm =  false;
		}else if (signUpCred.name.length < 1) {
			errorMessage = 'Please enter your name.'; 
			validForm =  false;
		}else{
			alert(signUpCred.email.length);
			validForm =  true;
		}		
		alert(validForm);
		
		if(validForm==false){
			sharedUtils.hideLoading(); 
			sharedUtils.showMessage(ErrorTitle, errorMessage , 1500);
		}
		/*
		
		//var valid = Auth.ValidateSignUpForm(signUpCred);
		
		/*
		var errorCode = error.code;
        var errorMessage = error.message;
        // [START_EXCLUDE]
        if (errorCode == 'auth/weak-password') {
          alert('The password is too weak.');
        } else {
          alert(errorMessage);
        }*/
		
		//return true;*/
	};
	
  
  /**
   * Generic function to validate input
   */
  self.validateInput = function(inputValue, inputType) {
    var qVal = $q.defer();
    switch (inputValue) {
      case undefined:
        handleValidation("INPUT_UNDEFINED", false)
        break
      case null:
        handleValidation("INPUT_NULL", false)
        break
      case "":
        handleValidation("INPUT_NULL", false)
        break
      default: 
        handleValidation("INPUT_VALID", true)
        break
    }
    function handleValidation(code, pass){
      if(pass){
        qVal.resolve(code);
      } else {
        qVal.reject(code);
      }
    };
    return qVal.promise;
  };
   
  return self;
})
