// Ionic app App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'app' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'app.services' is found in services.js
// 'app.controllers' is found in controllers.js
angular.module('app', [
    'ionic',
    'ngCordova',
    'ngMap',
    'angularMoment',
    'angularRipple',
    'ionic-datepicker',
    'angularLazyImg',

    'app.controllers',
    'app.routes',
    'app.directives',
    'app.services',
    'app.constants',
    'firebase',
    'firebaseConfig',

    // auth and profile

    'app.services-auth',
    'app.controllers-account',
    'app.services-profile',

    //utils
    'app.services-functions',
    'app.services-codes',
    'app.services-utils'
  ])

  .config(function ($ionicConfigProvider, $sceDelegateProvider) {
    $ionicConfigProvider.backButton.text('').previousTitleText(false);
    $sceDelegateProvider.resourceUrlWhitelist(['self', '*://www.youtube.com/**', '*://player.vimeo.com/video/**']);
    $ionicConfigProvider.tabs.position('bottom');
  })

  .run(function ($ionicPlatform, $rootScope, $state, $log, Popup, $timeout) {

    firebase.auth().onAuthStateChanged((user) => {
      $ionicPlatform.ready(function () {
        if (navigator.splashscreen) $timeout(function () {
          navigator.splashscreen.hide();
        }, 300);

        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);

          /*var permissions = cordova.plugins.permissions;
          		permissions.hasPermission(permissions.ACCESS_COARSE_LOCATION, function( status ){
          			if ( status.hasPermission ) {
          				console.log("Yes :D ");
          			}
          			else {
          				Popup.alert('Error', 'Error loading map. Please restart the app.', function() {
          					navigator.app.exitApp();
          				});
          			}
          		}); */


        }
        if (window.StatusBar) {
          StatusBar.styleDefault();
        }

        if (window.Connection) {
          if (navigator.connection.type == Connection.NONE) {
            Popup.alert('Error', 'You need internet connection to use this app. The app will now exit.', function () {
              navigator.app.exitApp();
            });
          }
        }
      });
    })



    /*$rootScope.$on("$routeChangeSuccess", function(currentRoute, previousRoute){
      //Change page title, based on Route information
      $rootScope.title = $route.current.title;
    });*/

    /*//stateChange event
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams, Auth){
    	alert(Auth.getAuthState());
    	if (!Auth.getAuthState()){ //Assuming the AuthService holds authentication logic
    		// User isn’t authenticated
    		$state.transitionTo("home");
    		event.preventDefault(); 
    	}
    }); */
  })


  .run(function ($rootScope, $state, AuthService, AUTH_EVENTS, Profile) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
			const firebaseUser = firebase.auth().currentUser
			const unAuthenticatedRoutes = ['home', 'signUp', 'verifyEmail', 'login']
			if (firebaseUser) {
				if (unAuthenticatedRoutes.includes(toState.name)) {
					$state.go('menu.courts');
				}
			} else {
				if (!unAuthenticatedRoutes.includes(toState.name)) {
					event.preventDefault();
					// $state.go('home');
				}
			}
      if (toState.redirectTo) {
        event.preventDefault();
        $state.go(toState.redirectTo, toParams);
      }
    });

    /*$rootScope.$on('$stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams) {
    	$state.current = toState;
    	 
    	alert($state.current.name);
      }
    )*/

    // Redirect the user to the login state if unAuthenticated
    /* $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
	  alert(toState);
    //console.log("$stateChangeError", error);
    event.preventDefault();
    if(error == "AUTH_LOGGED_OUT") {
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
      $state.go('home');
    }
  });*/

    /*$rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {

    if ('data' in next && 'authorizedRoles' in next.data) {
      var authorizedRoles = next.data.authorizedRoles;
      if (!AuthService.isAuthorized(authorizedRoles)) {
        event.preventDefault();
        $state.go($state.current, {}, {reload: true});
        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
      }
    }
	var test = AuthService.isAuthenticated();*/


    /*
	AuthService.getAuthState().then(function(user){  
	 

			 
				 //alert(provider);
				 //sharedUtils.showMessage('',JSON.stringify(user,undefined,4) , 3000); 
				//var userInfo = user.providerUserInfo()[0];
	 
				/*
				var profile = Profile.getById(user.uid);
				
				
				profile.$loaded().then(
					function(profileData){
						 //alert(profileData.full_name);
						//emailVerified = profileData.emailVerified;
						//alert(emailVerified);
						
						
						//providerUserInfo
						/*
						if(emailVerified){
							$ionicHistory.nextViewOptions({
								historyRoot: true,
								disableAnimate: true,
								disableBack: true
							}); 
							$ionicSideMenuDelegate.canDragContent(true);  // Sets up the sideMenu dragable
							$rootScope.extras = true; 
							sharedUtils.hideLoading();
							$state.go('menu.courts', {}, {location: "replace"});
						}else{
							
						}

					} ,function(error){
						alert(error);
						
					}
				)  
				//sharedUtils.hideLoading(); 
	}, function(err) { 
		//alert(next.name ); 
		var anonPages = ["home", "login","signUp","verifyEmail"];
		var index = anonPages.indexOf(next.name);
 
		if(index < 0){
			$state.go('home');
		} 
    });  */

    /*
    if (!AuthService.isAuthenticated()) {
      if (next.name == 'login') {
		 event.preventDefault();
         $state.go('login');
      }else if(next.name !== 'home' && next.name !== 'login'){
		 event.preventDefault();
         $state.go('home');  
	  }
    }
  });*/
  });


/*



.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});

*/

