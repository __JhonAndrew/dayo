angular.module('app.controllers', [])

  .controller('AppCtrl', function ($scope, $state, $ionicPopup, AuthService, AUTH_EVENTS) {

    /*$scope.email = AuthService.email();

    $scope.$on(AUTH_EVENTS.notAuthorized, function(event) {
      var alertPopup = $ionicPopup.alert({
        title: 'Unauthorized!',
        template: 'You are not allowed to access this resource.'
      });
    });

    $scope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
      AuthService.logout();
      $state.go('home');
      var alertPopup = $ionicPopup.alert({
        title: 'Session Lost!',
        template: 'Sorry, You have to login again.'
      });
    });

    $scope.setCurrentUsername = function(email) {
      $scope.userProfile = userProfile;
    };*/
  })

  .controller('MenuCtrl', function ($scope) {})

  .controller('CourtsCtrl', function ($scope, $state, Courts, GameDetails, Games, $cordovaGeolocation, NgMap, $ionicLoading, $ionicHistory, AuthService, sharedUtils, $timeout, Popup) {

    $scope.$on('$ionicView.beforeEnter', function (e) {
      $scope.AuthData = AuthService.AuthData;
    });

    $scope.logout = function () {
      AuthService.logout();
      $state.go('home');
    };

    $scope.directionsArray = []

    $scope.searchBar = false
    $scope.toggleSearchBar = function () {
      $scope.searchBar = !$scope.searchBar
    }

    $scope.userLocation = {
      lat: '',
      lng: ''
    };
    $scope.searchCourt = '';
    $scope.courts = Courts.get();
    $scope.courts.$loaded().then(courtDetails => {
      courtDetails.forEach((court) => {
        if (!court.hasOwnProperty('imageUrl') || !court.imageUrl) {
          // add thumbnail if no imageUrl prop
          const imageUrl = 'img/dayo_logo.jpg'
          const courtPosition = $scope.courts.findIndex((c) => c.id === court.id)
          if (courtPosition >= 0) {
            console.log($scope.courts[courtPosition])
            $scope.courts[courtPosition].imageUrl = imageUrl
          }
        }
        Games.getByCourtId(court.id).$loaded(games => {
          games.forEach((game) => {
            GameDetails.getByTeamId(game.$id).$loaded(gamedetails => {
              gamedetails.forEach(gd => {
                if (gd) {
                  court.booked = 1;
                  $scope.courts.push(game);
                }
              })
            });
          });
        });
      });
    }).catch(err => console.error(err));

    $timeout(() => {
      if (NgMap) {
        NgMap.getMap().then(function (map) {
          $scope.map = map;
          $scope.zoom = 12;
        });
      } else {
        Popup.alert('Error', 'Error loading map. Please restart the app.', () => {
          navigator.app.exitApp();
        });
      }
    }, 1500);

    new autoComplete({
      selector: 'input[id="searchCourtInput"]',
      minChars: 2,
      source(term, suggest) {
        const courts = filteredCourts().filter(c => !c.name.includes("bound"))
        term = term.toLowerCase();
        let choices = courts;
        let matches = []
        for (i = 0; i < choices.length; i++) {
          if (~choices[i].name.toLowerCase().indexOf(term)) {
            if (!term.includes('bound')) {
              matches.push(choices[i])
            }
          }
        }
        suggest(matches)
      },
      renderItem(item, search) {
        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        const re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
        return `
        <div class="autocomplete-suggestion" data-val="${item.name}" style="padding: 5px;">
          <img src="${item.imageUrl}" style="width: 7%; height: 7%; border-radius: 50%; vertical-align:middle;"> <strong>${item.name.replace(re, "<b>$1</b>")}</strong>
        </div>
      `
      },
      onSelect(e, term, item) {
        const courtIndex = filteredCourts().findIndex(c => c.name === term);
        const courtObject = filteredCourts()[courtIndex];
        $timeout(() => {
          $scope.map.setZoom(15);
          $scope.map.setCenter({
            lat: courtObject.lat,
            lng: courtObject.lng
          })
          $scope.court = courtObject;
          $scope.map.hideInfoWindow('infoWindowID');
          $scope.map.showInfoWindow('infoWindowID', $scope.court.$id);
        }, 10);
      }
    })

    const filteredCourts = () => {
      let courtsArray = Object.keys($scope.courts).map((court, i) => {
        const courtScope = $scope.courts[court]
        return {
          name: courtScope.name,
          address: courtScope.address,
          lat: courtScope.lat,
          lng: courtScope.lng,
          imageUrl: courtScope.imageUrl,
          $id: courtScope.$id,
          open_hours_end: courtScope.open_hours_end,
          open_hours_start: courtScope.open_hours_start,
          status: courtScope.status,
          website: courtScope.website,
          merchant_id: courtScope.merchant_id,
          contact: courtScope.contact
        }
      })
      courtsArray = courtsArray.filter(c => {
        return c.name !== undefined
      })
      return courtsArray;
    }

    $scope.court = {};
    $scope.destination = {
      lat: '',
      lng: ''
    };

    $ionicLoading.show({
      template: '<ion-spinner icon="lines"></ion-spinner> <br/> Loading courts...'
    });

    $scope.courts.$loaded().then(function () {
      $ionicLoading.hide(); // hide loader when court markers loaded
    });


    $scope.mapStyle = function () {
      return [{
          elementType: 'geometry',
          stylers: [{
            color: '#242f3e'
          }]
        },
        {
          elementType: 'labels.text.stroke',
          stylers: [{
            color: '#242f3e'
          }]
        },
        {
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#fff'
          }]
        },
        {
          featureType: 'administrative.locality',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#ef771e'
          }]
        },
        {
          featureType: "administrative.land_parcel",
          stylers: [{
            visibility: "off"
          }]
        },
        {
          elementType: "labels.icon",
          stylers: [{
            visibility: "off"
          }]
        },
        {
          featureType: 'poi',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#d59563'
          }]
        },
        {
          featureType: 'poi.park',
          elementType: 'geometry',
          stylers: [{
            color: '#263c3f'
          }]
        },
        {
          featureType: 'poi.park',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#6b9a76'
          }]
        },
        {
          featureType: 'road',
          elementType: 'geometry',
          stylers: [{
            color: '#38414e'
          }]
        },
        {
          featureType: 'road',
          elementType: 'geometry.stroke',
          stylers: [{
            color: '#212a37'
          }]
        },
        {
          featureType: 'road',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#9ca5b3'
          }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry',
          stylers: [{
            color: '#746855'
          }]
        },
        {
          featureType: 'road.highway',
          elementType: 'geometry.stroke',
          stylers: [{
            color: '#1f2835'
          }]
        },
        {
          featureType: 'road.highway',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#f3d19c'
          }]
        },
        {
          featureType: 'transit',
          elementType: 'geometry',
          stylers: [{
            color: '#2f3948'
          }]
        },
        {
          featureType: 'transit.station',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#d59563'
          }]
        },
        {
          featureType: 'water',
          elementType: 'geometry',
          stylers: [{
            color: '#010c1c'
          }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.fill',
          stylers: [{
            color: '#515c6d'
          }]
        },
        {
          featureType: 'water',
          elementType: 'labels.text.stroke',
          stylers: [{
            color: '#17263c'
          }]
        }
      ];
    }

    $scope.showDetails = function (e, court) {
      $scope.destination = {
        lat: '',
        lng: ''
      }
      $scope.court = court;
      $scope.searchBar = true;
      $scope.map.hideInfoWindow('infoWindowID');
      $scope.map.showInfoWindow('infoWindowID', $scope.court.$id);
    };

    const posOptions = {
      timeout: 10000,
      enableHighAccuracy: true
    };

    if (navigator.geolocation) {
      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then((position) => {
          const lat = position.coords.latitude
          const lng = position.coords.longitude
          $scope.userLocation = {
            lat,
            lng
          }
        }, (err) => {
          $scope.userLocation = {
            lat: 14.609054,
            lng: 121.022257
          }
          Popup.alert('Error', 'Unable to locate position.', function () {
            console.log("set to metro manila");
          });
        });
    } else {
      $scope.userLocation = {
        lat: 14.609054,
        lng: 121.022257
      }
      Popup.alert('Error', 'Unable to locate position.', function () {
        console.log("set to metro manila");
      });
    }

    $scope.viewBookedGames = function (court_id) {
      $state.go('menu.court-detail', {
        court_id
      });
    };

    $scope.createGame = function (court_id) {
      $state.go('menu.court-detail', {
        court_id
      });
    };

    $scope.showDirection = function (coords) {
      $scope.destination = {
        lat: coords[0],
        lng: coords[1]
      };
      console.log('destination', $scope.destination)
    };

    $scope.switchView = function (view) {
      if (view === "map") {
        $state.go('menu.courts');
      } else {
        $state.go('menu.court-list');
      }
    };

    $scope.doRefresh = function () {
      $timeout(() => {
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);
    };
  })

  .controller('CourtListCtrl', function ($scope, $state, Courts) {
    $scope.courts = Courts.get();
    $scope.searchCourt = '';
    $scope.viewBookedGames = function (court_id) {
      $state.go('menu.court-detail', {
        court_id: court_id
      });
    };
    $scope.switchView = function (view) {
      if (view === "map") {
        $state.go('menu.courts');
      } else {
        $state.go('menu.court-list');
      }
    };
  })

  .controller('CourtDetailCtrl', function ($scope, $state, Courts, $stateParams, $ionicModal, $timeout, Games, GameDetails, Teams, TeamUser, Popup, $ionicLoading, $ionicPopup, $cordovaDatePicker, AuthService, sharedUtils) {

    $scope.logout = function () {
      AuthService.logout();
      $state.go('home');
    };

    $scope.court = null

    $scope.AuthData = AuthService.AuthData;
    $scope.game_details = {};

    const court_id = $stateParams.court_id;
    $scope.court = Courts.getById(court_id);
    $scope.games = [];

    const date = new Date();
    const startTimeday = date.setHours(0, 0, 0, 0);
    const endTimeday = date.setHours(23, 59, 59, 999);

    Games.getByCourtId(court_id).$loaded(function (games) {
      games.forEach((game) => {
        // if (game.startTime >= startTimeday && game.endTime <= endTimeday) { // ORIGINAL
        if (game.startTime) { // TEMPORARY - FOR TESTING PURPOSE ONLY
          GameDetails.getByTeamId(game.$id).$loaded(function (gamedetails) {
            gamedetails.forEach((gd) => {
              if (gd) {
                game.booked = 1;
                game.title = gd.game_title;
                game.game_type = gd.game_type;
              }
            })
            game.status = 1;
            $scope.games.push(game);
          });
        }
      });
    });

    $scope.time_start = null;

    $scope.getTeams = function (game_id) {
      //var data = GameDetails.getById(game_id);
      //return data;
    };

    //console.log($scope.games);


    /*$scope.game = {
      type: "3v3",
      time_start: null,
      time_end: null,
      place: null,
      duration: 1,
      court_id: null,
      user_id: null, // user id here
      status: 1, // 0=done, 1=active
      created_at: Date.now()
    };*/

    // show a page loader before entering the page
    $ionicLoading.show({
      template: 'Loading...'
    });

    $timeout(() => {
      $scope.court.$loaded().then(function (data) {
        $ionicLoading.hide(); // clear page loader
        $scope.gamesLoaded = true;
        if(!$scope.court.hasOwnProperty('imageUrl')) {
          $scope.court.imageUrl = "img/dayo_logo.jpg"
        }
      });
    }, 250)

    $ionicModal.fromTemplateUrl('create-game-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.createGameModal = modal;
    });

    $scope.createGame = function () {
      $scope.createGameModal.show();
    };

    $scope.closeCreateGame = function () {
      $scope.createGameModal.hide();
    };

    $scope.$on('$destroy', function () {
      $scope.createGameModal.remove();
    });

    /*$scope.setGameTime = function(game_time) {
      $scope.time_start = game_time;
    };*/

    /*$scope.changeGameType = function(type) {
      $scope.game.type = type;
    };*/

    /*$scope.changeGameDuration = function(duration) {
      if(duration == 0) return false;
      $scope.game.duration = duration;
    };*/

    /*function getCurrentDate() {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      
      if(dd<10) {
          dd = '0'+dd;
      } 
      
      if(mm<10) {
          mm = '0'+mm;
      } 
      
      today = mm + '/' + dd + '/' + yyyy;
      return today;
    }*/

    /*$scope.addGame = function() {
      var start_time = `${getCurrentDate()} ${moment($scope.time_start).format('LT')}`;
      $scope.game.time_start = Date.parse(start_time);
      Popup.confirm('Are you sure?',`
      <p>Game Details</p>
      <p>Type: ${$scope.game.type}</p>
      <p>Place: ${$scope.game.place}</p>
      <p>Time: ${moment($scope.time_start).format('LT')}</p>
      <p>Duration: ${$scope.game.duration} hrs</p>
      `, function(res) {
        if(res) {
          Games.create($scope.game).then(function(ref) {
            Popup.alert('Game', 'Game successfully added.', function() {
              $scope.closeCreateGame();
            });
          });
        }
      });
    };*/

    $scope.changeGameType = function (type) {
      if (type === '3v3') {
        $scope.game_details.type = "3vs3";
      } else {
        $scope.game_details.type = "5vs5";
      }
    }

    $scope.createGame = function () {
      if (!$scope.game_details.name || !$scope.game_details.type) {
        Popup.alert('Book Game', 'All fields are required.', function () {});
      } else {
        Popup.alert('Book Game', 'Game successfully created.', function () {
          GameDetails.set({
            game_type: "5vs5",
            booked_at: Date.now(),
            game_id: game_id,
            game_name: $scope.game_details.name,
            is_team: 1
          });
          $ionicHistory.goBack();
        });
      }
    };

    $scope.viewGame = function (game_id, game_booked, game) {
      if (game_booked == 1) {
        // Popup.alert('Book Game', 'This game time slot already booked by other players.');
        $state.go('menu.team-players', {
          gameId: game_id
        })
      } else {
        $state.go('menu.game-detail', {
          game_id: game_id
        });
      }
    };

    $scope.doRefresh = function () {

      console.log('Refreshing!');
      $timeout(function () {
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');

      }, 1000);

    };
  })

  .controller('GameDetailCtrl', function ($scope, Courts, $stateParams, $state, $ionicModal, AuthService, Games, Popup, GameDetails, Teams, TeamUser, $ionicPopup, $ionicLoading, $ionicHistory) {

    $scope.AuthData = AuthService.AuthData;

    var game_id = $stateParams.game_id;

    $scope.game = Games.getById(game_id).$loaded((game) => {
      //console.log('g', game);
      Courts.getById(game.court_id).$loaded((court) => {
        $scope.game.court_id = game.court_id;
        $scope.game.court_name = court.name;
        $scope.game.court_address = court.address;
        $scope.game.game_price = game.price;
        $scope.game.startTime = game.startTime;
        $scope.game.endTime = game.endTime;
      })
    });

    $scope.selected_team = null;
    $scope.teams = [];

    $scope.has_team = false;

    $scope.gameCreated = false; // check if game is create so hide the form

    $scope.game_details = {
      type: null,
      name: null
    }

    $scope.joinGame = function () {
      /*if(!$scope.selected_team) {
        Popup.alert('Join Game', 'Please choose a team.', function() {});
        return false;
      }*/
      GameDetails.getByTeamId(game_id).$loaded(function (gd) {
        if (gd.length > 0) {
          var teams = gd[0].teams;
          const teamIndex = teams.findIndex(teamName => teamName === $scope.selected_team); // get index of selected team
          if (teamIndex >= 0) {
            Popup.alert('Join Game', 'Your team is already in the game.', function () {
              $ionicHistory.goBack();
            });
          } else {
            if (teams.length === 2) {
              Popup.alert('Join Game', 'Game is full.', function () {
                $ionicHistory.goBack();
              });
            } else {
              var record = GameDetails.getById(gd[0].$id);
              record.$loaded().then(function (r) {
                r.teams.push($scope.selected_team);
                record.$save().then(function (ref) {
                  Popup.alert('Join Game', 'Successfully joined the game.', function () {
                    $ionicHistory.goBack();
                  });
                });
              });
            }
          }
        }
      });
    };

    $scope.createGame = function () {

      //if(!$scope.selected_team) {
      //  Popup.alert('Create Game', 'Please choose a team.', function() {});
      // } else {
      //var teams = [];
      //teams.push($scope.selected_team);
      if (!$scope.game_details.name) {
        Popup.alert('Create Game', 'Please Name Your Game.', function () {});
      } else {
        //status 1 = waiting, 2 = full, 3 = playing, 4 - completed

        $scope.game_details.type = "5vs5";

        Popup.alert('Create Game', 'Court Successfull Booked.', function () {
          GameDetails.set({
            game_id: game_id,
            court_id: $scope.game.court_id,
            game_title: $scope.game_details.name,
            game_type: $scope.game_details.type,
            status: 1,
            is_team: 1,
            user_id: $scope.AuthData.uid,
            booked_at: Date.now()
            //teams: teams
          });
          // $ionicHistory.goBack();
          $state.go('menu.team-players', {
            gameId: game_id
          })
          $scope.$broadcast('scroll.refreshComplete');
        });
      }
      //}
    };

    $scope.changeTeam = function (selected_team) {
      $scope.selected_team = selected_team;
    };

    TeamUser.get().$watch(function (event) {
      var e = event.event;
      var key = event.key;
      if (e == "child_added") {
        TeamUser.getById(key).$loaded(function (tu) {
          $scope.teamsLoaded = true;
          $scope.teamOptions = true;
          if (tu.user_id == $scope.AuthData.uid) {
            Teams.getById(tu.team_id).$loaded(function (team) {
              $scope.teams.push(team); // add new team to team array
            });
          }
        });
      }
    });

    Teams.get().$watch(function (event) {
      var e = event.event;
      var key = event.key;
      if (e == "child_changed") {
        const index = $scope.teams.findIndex(obj => obj.$id === key); // get index of updated item in the array
        if (index >= 0) {
          Teams.getById(key).$loaded(function (teamData) {
            $scope.teams[index] = teamData;
          });
        }
      }
    });


  })

  .controller('TeamPlayersCtrl', function ($scope, $stateParams, $ionicModal, Teams, TeamUser, AuthService) {
    $scope.game = {}

    let gameDetails = firebase.database().ref('game_details').orderByChild('game_id').equalTo($stateParams.gameId)
    gameDetails.once('value', snapshot => {
      $scope.game = snapshot.val()
      Object.keys($scope.game).forEach(key => $scope.game = $scope.game[key])
      Object.keys(snapshot.val()).forEach((key) => {
        $scope.game.$id = key
      })
      if(!$scope.game.hasOwnProperty('teams')) $scope.game.teams = []
      console.log('loaded', $scope.game)
    })

    gameDetails.on('child_changed', (snapshot) => {
      $scope.game.teams = snapshot.val().teams
      console.log('current teams', $scope.game.teams)
    })

    $ionicModal.fromTemplateUrl('add-team-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.addTeamModal = modal;
    });

    $scope.AuthData = AuthService.AuthData;

    $scope.openAddTeam = function (position) {
      $scope.addTeamModal.show();
      /* if (position === 0) {
        $scope.addTeamModal.show();
      } else {

      } */
    }

    $scope.addMyTeam = function (team) {
      if(!team.hasOwnProperty('image')) team.image = 'img/dayo_logo.jpg'
      const gameDetails = firebase.database().ref(`game_details/${$scope.game.$id}`)
      const myTeam = {
        team_id: team.$id,
        image: team.image,
        name: team.name
      }
      $scope.game.teams.push(myTeam)
      console.log(myTeam)
      gameDetails.update({teams: $scope.game.teams})
      .then(() => {
        $scope.addTeamModal.hide();
      })
    }

    $scope.closeAddTeam = function () {
      $scope.addTeamModal.hide();
    }

    $scope.myTeams = []

    TeamUser.get().$watch(function (event) {
      var e = event.event;
      var key = event.key;
      if (e == "child_added") {
        TeamUser.getById(key).$loaded(function (tu) {
          if (tu.user_id == $scope.AuthData.uid) {
            Teams.getById(tu.team_id).$loaded(function (team) {
              if(team.hasOwnProperty('image')) {
                team.image = `data:image/jpeg;base64,${t.image}`
              }
              $scope.myTeams.push(team); // add new team to team array
            });
          }
        });
      }
    });
  })

  .controller('TeamsCtrl', function ($scope, $ionicModal, $state, Popup, Teams, TeamUser, Camera, AuthService) {
    $ionicModal.fromTemplateUrl('create-team-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.createTeamModal = modal;
    });

    $scope.logout = function () {
      AuthService.logout();
      $state.go('home');
    };

    $scope.team = {
      name: null,
      image: null,
      is_creator: 1,
      created_at: Date.now()
    };

    $scope.AuthData = AuthService.AuthData;

    $scope.teams = [];

    TeamUser.get().$watch(function (event) {
      var e = event.event;
      var key = event.key;
      if (e == "child_added") {
        TeamUser.getById(key).$loaded(function (tu) {
          if (tu.user_id == $scope.AuthData.uid) {
            Teams.getById(tu.team_id).$loaded(function (team) {
              $scope.teams.push(team); // add new team to team array
            });
          }
        });
      }
    });

    Teams.get().$watch(function (event) {
      var e = event.event;
      var key = event.key;
      if (e == "child_changed") {
        const index = $scope.teams.findIndex(obj => obj.$id === key); // get index of updated item in the array
        if (index >= 0) {
          Teams.getById(key).$loaded(function (teamData) {
            $scope.teams[index] = teamData;
          });
        }
      }
    });

    $scope.createTeam = function () {
      $scope.createTeamModal.show();
    };

    $scope.closeCreateTeam = function () {
      $scope.createTeamModal.hide();
    };

    $scope.$on('$destroy', function () {
      $scope.createTeamModal.remove();
    });

    $scope.uploadPicture = function () {
      Camera.gallery().then(function (imageURI) {
        $scope.team.image = imageURI;
      });
    };

    $scope.addTeam = function () {
      Teams.set($scope.team).then(function (ref) {
        TeamUser.set({
          team_id: ref.key,
          user_id: $scope.AuthData.uid, // user id here
          created_at: Date.now()
        });
        Popup.alert('Team', 'Team successfully added.', function () {
          $scope.closeCreateTeam();
          $scope.team = {
            name: null,
            image: null,
            created_at: Date.now()
          };
        });
      });
    };

    $scope.viewTeam = function (team_id) {
      $state.go('menu.team-detail', {
        team_id: team_id
      });
    };

    $scope.doRefresh = function () {

      console.log('Refreshing!');
      $timeout(function () {
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');

      }, 1000);

    };
  })

  .controller('TeamDetailCtrl', function ($scope, $stateParams, $ionicModal, $timeout, $ionicPopup, Popup, Teams, TeamUser, Members, AuthService) {

    $scope.logout = function () {
      AuthService.logout();
      $state.go('home');
    };

    $scope.AuthData = AuthService.AuthData;
    $scope.ProfileData = AuthService.ProfileData;

    var team_id = $stateParams.team_id;
    $scope.team = Teams.getById(team_id); // team details
    $scope.members = []; // members array
    var user_id = $scope.AuthData.uid;
    $scope.userData = false;
    $scope.userListData = false;
    $scope.u = {};
    $scope.position = null;
    $scope.u.search_criteria = 'name';
    $scope.users_found = [];

    // search user
    $scope.$watch('u.search_criteria', function () {
      $scope.userData = false;
      $scope.userListData = false;
      $scope.u.data = "";
      $scope.u.search = "";
    });

    $scope.$watch('u.search', function (did = "") {
      if ($scope.u.search_criteria === 'did') {
        if (did.length == 12) {
          console.log($scope.ProfileData);
          didUpper = did.toUpperCase();
          Members.get().$loaded(function (members) {
            members.forEach(function (member, i) {
              //if(member.did == didUpper && member.did != $scope.ProfileData.did) {
              if (member.did == didUpper) {
                $scope.userData = true; // show userData
                $scope.u.data = member;
              }
            });
          });
        } else {
          $scope.userData = false;
          $scope.u.data = "";
        }
      } else if ($scope.u.search_criteria === 'name') {
        if (did.length > 0) {
          $scope.users_found = [];
          Members.get().$loaded(function (members) {
            $scope.userListData = false;
            members.forEach(function (member, i) {
              var full_name = member.full_name.toLowerCase();
              if (full_name.includes(did.toLowerCase())) {
                $scope.users_found.push(member);
                $scope.userListData = true;
                console.log(member);
              }
            });
          });
        } else {
          $scope.users_found = [];
          $scope.userListData = false;
        }
      }
    });

    $scope.addMember = function (user_id) {

      TeamUser.set({
        team_id: $scope.team.$id,
        user_id: user_id, // user id here
        is_creator: 0,
        created_at: Date.now()
      });

      Popup.alert('Add Member', 'User successfully added to team.', function () {
        $scope.closeAddMemberModal();
        $scope.u = {
          data: {},
          search: ''
        };
      });

      /*
      TeamUser.getByTeamId(team_id).$loaded(function(tu) {
        if(tu.length >= 3) {
        Popup.alert('Add Member', 'Sorry, a team can only have 3 members.', function() {
          $scope.closeAddMemberModal();
          $scope.u = {
          data: {},
          search: ''
          };
        });
        } else { 
        }
      });*/
    };

    $scope.removeUser = function (user_id) {
      Popup.confirm('User', 'Remove the user from the team?', function (res) {
        if (res) {
          TeamUser.getByTeamId(team_id).$loaded(function (teamUserData) {
            teamUserData.forEach(function (tu, index) {
              if (tu.user_id == user_id) {
                var record = TeamUser.getById(tu.$id);
                record.$remove();
              }
            });
          });
        }
      });
    };

    $scope.updatePosition = function (user_id) {
      var showPopup = $ionicPopup.show({
        template: `
        <div class="list">
        
        <label class="item item-input item-select">
          <div class="input-label">
          Position
          </div>
          <select ng-model="u.position">
          <option>Point Guard</option>
          <option>Shooting Guard</option>
          <option>Small Forward</option>=
          <option>Power Forward</option>
          <option>Center</option>  
          </select>
        </label>
        
        </div>
        `,
        title: 'User',
        subTitle: 'Update Position',
        scope: $scope,

        buttons: [{
          text: 'Cancel'
        }, {
          text: '<b>Save</b>',
          type: 'button-positive',
          onTap: function (e) {

            if (!$scope.u.position) {
              //don't allow the user to close unless he enters model...
              e.preventDefault();
            } else {
              console.log($scope.u.position);
              TeamUser.getByTeamId(team_id).$loaded(function (teamUserData) {
                teamUserData.forEach(function (tu, index) {
                  if (tu.user_id == user_id) {
                    var record = TeamUser.getById(tu.$id);
                    record.$loaded().then(function () {
                      record.position = $scope.u.position;
                      record.$save().then(function (ref) {
                        console.log("Saved");
                      });
                    });
                  }
                });
              });
            }
          }
        }]
      });
    };

    $ionicModal.fromTemplateUrl('search-member-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.searchMemberModal = modal;
    });

    $scope.openAddMemberModal = function () {
      $scope.searchMemberModal.show();
    };

    $scope.closeAddMemberModal = function () {
      $scope.searchMemberModal.hide();
    };

    $scope.$on('$destroy', function () {
      $scope.searchMemberModal.remove();
    });

    // check if authenticated user is the creator of team
    TeamUser.getMembersByTeamId(team_id).$loaded(function (teams) {
      teams.forEach(function (team, i) {
        if (team.user_id == user_id) {
          if (team.is_creator == 1) $scope.is_creator = true; // show add team member button if true
        }
      });
    });

    // get all the members of the team
    TeamUser.getMembersByTeamId(team_id).$watch(function (event) {
      var e = event.event;
      var key = event.key;
      if (e == "child_added") {
        TeamUser.getById(key).$loaded(function (tu) {
          Members.getById(tu.user_id).$loaded(function (member) {
            member.position = tu.position;
            $scope.members.push(member);
          });
        });
      } else if (e == "child_changed") {
        TeamUser.getById(key).$loaded(function (tu) {
          Members.getById(tu.user_id).$loaded(function (member) {
            let memberIndex = $scope.members.map(obj => obj.$id).indexOf(tu.user_id);
            $scope.members[memberIndex].position = tu.position;
          });
        });
      }
    });


    // check if there's an update in the members profile
    Members.get().$watch(function (event) {
      var e = event.event;
      var key = event.key;
      if (e == "child_changed") {
        Members.getById(key).$loaded(function (member) {
          var memberIndex = $scope.members.map(obj => obj.$id).indexOf(key); // get index of updated member in the array
          if (memberIndex >= 0) {
            $scope.members[memberIndex] = member;
          }
        });
      }
    });


    $scope.doRefresh = function () {

      console.log('Refreshing!');
      $timeout(function () {
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');

      }, 1000);

    };

  })

  .controller('ShopCtrl', function ($scope) {
    $scope.logout = function () {
      AuthService.logout();
      $state.go('home');
    };
  })

  .controller('ChatsCtrl', function ($scope, $ionicModal, Chats, Members, $state) {

    $ionicModal
      .fromTemplateUrl('search-user-modal', {
        scope: $scope,
        animation: 'slide-in-up'
      })
      .then(modal => $scope.searchModal = modal)

    $scope.search = {
      criteria: 'did',
      results: []
    }

    $scope.chats = Chats.all()

    

    $scope.goToConvo = function (id, name) {
      $state.go('menu.chat-detail', {
        chatId: id,
        name
      });
    }

    $scope.$watch('search.criteria', () => {
      $scope.search.text = ''
      $scope.search.results = []
    })

    // Search Friend
    $scope.$watch('search.text', () => {
      let {
        criteria,
        text,
        results
      } = $scope.search

      if (text.length > 0) {
        console.log('Searching...')

        Members.get().$loaded(members => {
          $scope.search.results = members.filter(member => {
            return (criteria === 'full_name') ? member.full_name.toUpperCase().includes(text.toUpperCase()) : member.did === text.toUpperCase()
          })
          console.log('Done searching.')
        })
      } else $scope.search.results = []
    })

    // Add Friend
    $scope.addFriend = user => {
      console.log('Adding user:', user)
      $scope.searchModal.hide()
      $scope.search.results = []

      Chats.addFriend({
        uid: user.$id,
        full_name: user.full_name,
        photo: user.photo || null
      }).then(() => {
        console.log('Adding friend done.')
      })
    }
  })

  .controller('ChatDetailCtrl', function ($scope, $timeout, $stateParams, Chats) {
    let scrollToBot = () => {
      let convElem = document.querySelector('#conversation')
      convElem.scrollTop = convElem.scrollHeight
    }

    $scope.chatId = $stateParams.chatId
    $scope.conversation = []
    $scope.chatName = $stateParams.name

    Chats.getConversation($stateParams.chatId).on('child_added', snapshot => {
      let message = snapshot.val()
      message.formattedTime = moment(message.timestamp).format('h:mm a, MMMM D, YYYY')
      $scope.conversation.push(message)
      scrollToBot()
    })

    $scope.sendMessage = () => {
      Chats.sendMsg({
        recepient: $stateParams.chatId,
        message: $scope.message,
        timestamp: Date.now()
      }).then(() => $scope.message = '')
    }

    $timeout(() => scrollToBot(), 100)
  });

