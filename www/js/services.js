angular.module('app.services', [])

.factory('Popup', function($ionicPopup) {
  return {
    alert: function(title, message, callback) {
      $ionicPopup.alert({
        title: title,
        template: message
      }).then(callback);
    },
    confirm: function(title, message, callback) {
      var confirmPopup = $ionicPopup.confirm({
        title: title,
        template: message
      }).then(callback);
    }
  };
})
 
.factory('fireBaseData', function($firebase) {
	var ref = new Firebase("https://dayo-app-a9b37.firebaseio.com/"), 
    refUser = new Firebase("https://dayo-app-a9b37.firebaseio.com/users");
  return {
    ref: function() {
      return ref;
    }, 
    refFirebaseDBUser: function() {
      return refUser;
    }, 
  }
})


.factory('sharedUtils', function($ionicLoading,$ionicPopup,$timeout){


    var functionObj={};

    functionObj.showLoading=function(message){
		if(message){ 
		  $ionicLoading.show({
			template: message,
			animation: 'fade-in',
			showBackdrop: true,
			showDelay: 0
		  });	
		}else{
		  $ionicLoading.show({
			content: '<i class=" ion-loading-c"></i> ', // The text to display in the loading indicator
			animation: 'fade-in', // The animation to use
			showBackdrop: true, // Will a dark overlay or backdrop cover the entire view
			maxWidth: 200, // The maximum width of the loading indicator. Text will be wrapped if longer than maxWidth
			showDelay: 0 // The delay in showing the indicator
		  });
		}
    };
    functionObj.hideLoading=function(){
      $ionicLoading.hide();
    };


    functionObj.showMessage = function(title,message,optHideTime,cssClass,onTap) {

		if(onTap){ 
			var alertPopup = $ionicPopup.alert({
				title: title,
				template: '<span class="confirmBox">'+message+'</span>',
				cssClass:'ErrorAlert',
				buttons: [onTap]				
			}); 
		}else{
			var alertPopup = $ionicPopup.alert({
				title: title,
				template: message,
				cssClass:'ErrorAlert'
			});
		}
		if(optHideTime != undefined && optHideTime > 100) {		  
			$timeout(function(){
			  $ionicLoading.hide();
			}, optHideTime)
		}
    };

    return functionObj;

})

.factory('Courts', function($firebaseArray, $firebaseObject) {
  var courtsRef = firebase.database().ref().child('courts');
  return {
    get: function() {
	  var courts = courtsRef.orderByChild('status').equalTo('1'); //get all courts
      return $firebaseArray(courts)	  
    },
    getById: function(court_id) {
      var court = courtsRef.child(court_id);
      return $firebaseObject(court);
    }
  };
})

.factory('Games', function($firebaseArray, $firebaseObject) {
  var gamesRef = firebase.database().ref().child('games');
  return {
    get: function() {
      return $firebaseArray(gamesRef); // get all games
    },
    getById: function(game_id) {
      var game = gamesRef.child(game_id);
      return $firebaseObject(game);
    },
    getByCourtId: function(court_id) {
      var gamesByCourtId = gamesRef.orderByChild('court_id').equalTo(court_id);
      return $firebaseArray(gamesByCourtId); // get games in select court
    },
    create: function(gameData) {
      var games = $firebaseArray(gamesRef);
      return games.$add(gameData);
    }
  };
})

.factory('GameDetails', function($firebaseArray, $firebaseObject) {
  var gameDetailsRef = firebase.database().ref().child('game_details');
  return {
    get: function() {
      return $firebaseArray(gameDetailsRef);
    },
    getByTeamId: function(game_id) {
      var gameById = gameDetailsRef.orderByChild('game_id').equalTo(game_id);
      return $firebaseArray(gameById);
    },
    getById: function(game_details_id) {
      var gameDetailsId = gameDetailsRef.child(game_details_id);
      return $firebaseObject(gameDetailsId);
    },
    set: function(game_detail_data) {
      var game_detail = $firebaseArray(gameDetailsRef);
      return game_detail.$add(game_detail_data);
    }
  };
})

.factory('Popup', function($ionicPopup) {
  return {
    alert: function(title, message, callback) {
      $ionicPopup.alert({
        title: title,
        template: message
      }).then(callback);
    },
    confirm: function(title, message, callback) {
      var confirmPopup = $ionicPopup.confirm({
        title: title,
        template: message
      }).then(callback);
    }
  };
})

.factory('UserProfile', function($firebaseArray, $firebaseObject) {
  var userRef = firebase.database().ref().child('users');  
  return {
    get: function() {
      return $firebaseArray(userRef);
    },
    getById: function(user_id) {
      var userId = userRef.child(user_id);
      return $firebaseObject(userId);
    } 
  };
})

.factory('Teams', function($firebaseArray, $firebaseObject) {
  var teamsRef = firebase.database().ref().child('teams');
  return {
    get: function() {
      return $firebaseArray(teamsRef); // get all teams
    },
    getById: function(team_id) {
      var team = teamsRef.child(team_id);
      return $firebaseObject(team);
    },
    set: function(teamData) {
      var team = $firebaseArray(teamsRef);
      return team.$add(teamData);
    }
  };
})

.factory('TeamUser', function($firebaseArray, $firebaseObject) {
  var teamUserRef = firebase.database().ref().child('team_user');
  var teamsRef = firebase.database().ref().child('teams');
  return {
    get: function() {
      return $firebaseArray(teamUserRef);
    },
    getById: function(team_user_id) {
      var teamUserId = teamUserRef.child(team_user_id);
      return $firebaseObject(teamUserId);
    },
    getByTeamId: function(team_id) {
      var getByteamId = teamUserRef.orderByChild('team_id').equalTo(team_id);
      return $firebaseArray(getByteamId);
    },
    getMembersByTeamId: function(team_id) {
      var membersByTeamId = teamUserRef.orderByChild('team_id').equalTo(team_id);
      return $firebaseArray(membersByTeamId); // get games in select court
    },
    set: function(teamUserData) {
      var teamUser = $firebaseArray(teamUserRef);
      return teamUser.$add(teamUserData);
    }
  };
})

.factory('Members', function($firebaseArray, $firebaseObject) {
  var userRef = firebase.database().ref().child('users');
  return {
    get: function() {
      return $firebaseArray(userRef);
    },
    getById: function(user_id) {
      var userId = userRef.child(user_id);
      return $firebaseObject(userId);
    },
    getByDid: function(did) {
      var userDid = userRef.orderByChild('did').equalTo(did);
      return $firebaseObject(userDid);
    }
  };
})

.factory('Camera', function($cordovaCamera) {
  return {
    capture: function() {
      var cameraOptions = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 100,
        targetHeight: 100,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation:true
      };
      return $cordovaCamera.getPicture(cameraOptions);
    },
    gallery: function() {
      var galleryOptions = {
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      };
      return $cordovaCamera.getPicture(galleryOptions);
    }
  };
  
})

.factory('Chats', ($firebaseArray, $firebaseObject) => {
  return {
    // Temp
    all () {
      const {currentUser} = firebase.auth()
      const friendsList = firebase.database().ref(`/friends/${currentUser.uid}`)
      return $firebaseArray(friendsList)
    },
    // Reference: https://stackoverflow.com/a/44206741
    addFriend (friend) {
      return new Promise((resolve, reject) => {
        const {currentUser} = firebase.auth()
        const friendsRef = firebase.database().ref(`/friends/${currentUser.uid}/${friend.uid}`)

        resolve(friendsRef.set({
          id: friend.uid,
          name: friend.full_name,
          face: friend.photo || '',
          lastText: ''
        }))
      })
    },
    // Reference: https://stackoverflow.com/a/44206741
    sendMsg (message) {
      return new Promise((resolve, reject) => {
        const {currentUser} = firebase.auth()
        const msgRefs = [
          // Sender
          $firebaseArray(firebase.database().ref(`/messages/${currentUser.uid}/${message.recepient}`)),
          // Recepient
          $firebaseArray(firebase.database().ref(`/messages/${message.recepient}/${currentUser.uid}`))
        ]

        // Send message
        msgRefs.forEach(ref => ref.$add(message))

        // Update your data in recepients friends list incase you have changes
        firebase.database().ref(`/users/${currentUser.uid}`).once('value', snapshot => {
          let yourAccount = snapshot.val()

          firebase.database().ref(`/friends/${message.recepient}/${currentUser.uid}`).set({
            id: currentUser.uid,
            name: yourAccount.full_name,
            face: yourAccount.photo || '',
            lastText: message.message 
          })
        })

        // Update your friends list
        firebase.database().ref(`/friends/${currentUser.uid}/${message.recepient}/lastText`).set(message.message)

        resolve()
      })
    },
    getConversation (recepientId) {
      const {currentUser} = firebase.auth()
      return firebase.database().ref(`/messages/${currentUser.uid}/${recepientId}`)
    }
  }
});